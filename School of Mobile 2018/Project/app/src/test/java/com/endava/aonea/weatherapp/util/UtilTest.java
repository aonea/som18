package com.endava.aonea.weatherapp.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class UtilTest {

    @Test
    public void getDayFromEpochTest() {
        assertEquals("SATURDAY", Util.getDayFromEpoch("1526144400"));
    }

    @Test
    public void getDdMMTest() {
        assertEquals("06/05", Util.getDdMm("2018-05-06"));
    }

    @Test
    public void fahrenheitToCelsiusTest() {
        assertEquals(37.77778, Util.fahrenheitToCelsius(100), 0.00001);
    }
}