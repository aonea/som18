package com.endava.aonea.weatherapp.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import com.endava.aonea.weatherapp.http.request.service.CurrentConditionsService;

import java.util.Calendar;

public class CurrentConditionsAlarm {
    private Intent intent;
    private static PendingIntent pendingIntent;
    private static AlarmManager alarmManager;
    private Context context;

    public CurrentConditionsAlarm(Context context) {
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        this.context = context;
        intent = new Intent(context, CurrentConditionsService.class);
        intent.putExtra("caller", "boot_receiver");
        pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    public void setupAlarm(long interval) {
        boolean alarmExists = PendingIntent.getService(
                context, 0, intent, PendingIntent.FLAG_NO_CREATE) == null;

        if (!alarmExists) {
            Log.i("CCAlarm", "SETTING UP ALARM");
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(),
                    interval, pendingIntent);
        }
    }

    public void resetAlarm(long interval) {
        alarmManager.cancel(pendingIntent);
        setupAlarm(interval);
    }

    public void cancelAlarm() {
        alarmManager.cancel(pendingIntent);
    }
}
