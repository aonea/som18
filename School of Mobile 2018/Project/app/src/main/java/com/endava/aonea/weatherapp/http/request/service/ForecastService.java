package com.endava.aonea.weatherapp.http.request.service;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.endava.aonea.weatherapp.R;
import com.endava.aonea.weatherapp.http.connection.ConnectionWrapper;
import com.endava.aonea.weatherapp.http.request.RequestManager;
import com.endava.aonea.weatherapp.http.response.ForecastResponseManager;
import com.endava.aonea.weatherapp.model.MainInfoModel;
import com.endava.aonea.weatherapp.ui.activity.MainActivity;
import com.endava.aonea.weatherapp.ui.recyclerview.DayItem;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ForecastService extends IntentService {
    public static final String ACTION_FORECAST_UPDATE = "ACTION_FORECAST_UPDATE";
    private static final String CHANNEL_ID = "1";
    private static final int NOTIFICATION_ID = 1;
    public static List<DayItem> forecastList;
    NotificationChannel notificationChannel;
    private String locationName;
    private Map<String, String> codeCityMap = new HashMap<>();
    ForecastResponseManager forecastResponseManager;
    ConnectionWrapper forecastConnectionWrapper;

    public ForecastService() {
        super("ForecastService");
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public ForecastService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        forecastConnectionWrapper = new ConnectionWrapper(RequestManager.getRequestUrlForecast(
                 MainActivity.locationCode
        ));
        try {
            forecastConnectionWrapper.getConnection().connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        forecastResponseManager = new ForecastResponseManager(forecastConnectionWrapper.httpURLConnection);
        getLocations();
        locationName = MainInfoModel.getInstance().getLocationName();
        createNotificationChannel();
        getForecastList(intent);
        try {
            notifyUser();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getForecastList(Intent intent) {

        try {
            forecastList = forecastResponseManager.getDayList();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent updateIntent = new Intent(ACTION_FORECAST_UPDATE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(updateIntent);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Forecast",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
        }
    }

    private void notifyUser() throws JSONException {
        PendingIntent contentIntent = PendingIntent.getActivity(
                this,
                0,
                new Intent(this, MainActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        notificationManager.notify(
                NOTIFICATION_ID,
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentText("New updates for " + locationName)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Forecast")
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(forecastResponseManager.getHeadline()))
                        .build()
        );
    }

    private void getLocations(){
        //mocking a db query, or a http query
        codeCityMap.put(MainActivity.IASI_LOCATION_CODE, "Iasi");
        codeCityMap.put(MainActivity.BERN_LOCATION_CODE, "Bern");
        codeCityMap.put(MainActivity.PARIS_LOCATION_CODE, "Paris");
    }
}
