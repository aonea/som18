package com.endava.aonea.weatherapp.http.request.service;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.endava.aonea.weatherapp.http.connection.ConnectionWrapper;
import com.endava.aonea.weatherapp.http.request.RequestManager;
import com.endava.aonea.weatherapp.http.response.CurrentConditionsResponseManager;
import com.endava.aonea.weatherapp.model.MainInfoModel;
import com.endava.aonea.weatherapp.ui.activity.MainActivity;
import com.endava.aonea.weatherapp.ui.notification.CurrentConditionsNotificationWrapper;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
//d352e3bd8f081e0fe49d5b40e11fa29c
//http://api.openweathermap.org/data/2.5/weather?q=Paris,France&appid=d352e3bd8f081e0fe49d5b40e11fa29c

//TODO: abstractisize services
public class CurrentConditionsService extends IntentService {
    MainInfoModel mainInfoModel = MainInfoModel.getInstance();
    CurrentConditionsNotificationWrapper currentConditionsNotificationWrapper;
    private String locationName;


    public CurrentConditionsService() {
        super("CurrentConditionsService");
    }

    public CurrentConditionsService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        currentConditionsNotificationWrapper = new CurrentConditionsNotificationWrapper(this);
        String caller = "";

        Bundle bundle = null;
        if (intent != null) {
            bundle = intent.getExtras();
        }
        if (bundle != null) {
            caller = intent.getStringExtra("caller");
            sendMessage(bundle);
        }

        getDataForMainModel();

        if (caller != null) {
            if (caller.equals("boot_receiver")) {
                if (getSharedPreferences("my_shared_pref", Context.MODE_PRIVATE).getBoolean("notifications_on_boot", true)) {
                    notifyUser();
                }
            }
        }
    }

    private void sendMessage(Bundle bundle) {
        Messenger messenger = (Messenger) bundle.get("messenger");
        Message message = Message.obtain();

        if (messenger == null) return;

        Bundle newBundle = new Bundle();
        getDataForMainModel();

        try {
            messenger.send(message);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void getDataForMainModel() {
        ConnectionWrapper currentConditionsConnection;
        locationName = this.getSharedPreferences("my_shared_pref", Context.MODE_PRIVATE)
                .getString("city_name", "Bucharest, Romania");

        currentConditionsConnection = new ConnectionWrapper(RequestManager.getRequestUrlCurr(locationName));

        try {
            currentConditionsConnection.getConnection().connect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        CurrentConditionsResponseManager currentDayResponseManager =
                new CurrentConditionsResponseManager(currentConditionsConnection.httpURLConnection);

        mainInfoModel.setDay(currentDayResponseManager.getDay());
        mainInfoModel.setDate(currentDayResponseManager.getDate());
        mainInfoModel.setCurrentTemp(currentDayResponseManager.getCurrentTemp());
        mainInfoModel.setTime(currentDayResponseManager.getTime());
        mainInfoModel.setDescription(currentDayResponseManager.getDescription());
        mainInfoModel.setHumidity(currentDayResponseManager.getHumidity());
        mainInfoModel.setWind(currentDayResponseManager.getWind());
        mainInfoModel.setPressure(currentDayResponseManager.getPressure());
    }

    private void notifyUser() {
        currentConditionsNotificationWrapper.notifyUser(
                locationName,
                mainInfoModel.getCurrentTemp(),
                mainInfoModel.getDescription()
        );
    }
}