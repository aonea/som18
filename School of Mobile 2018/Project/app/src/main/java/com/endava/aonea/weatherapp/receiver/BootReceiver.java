package com.endava.aonea.weatherapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.endava.aonea.weatherapp.model.MainInfoModel;
import com.endava.aonea.weatherapp.ui.activity.MainActivity;
import com.endava.aonea.weatherapp.ui.activity.SettingsActivity;
import com.endava.aonea.weatherapp.alarm.CurrentConditionsAlarm;

public class BootReceiver extends BroadcastReceiver {
    public static final String ID = "boot_receiver";
    static MainInfoModel mainInfoModel = MainInfoModel.getInstance();

    public BootReceiver() {
    }


    @Override
    public void onReceive(Context context, Intent intent) {

        updateModel(context);

        String intentAction = intent.getAction();
        CurrentConditionsAlarm currentConditionsAlarm = new CurrentConditionsAlarm(context);
        Log.i("BootReceiver", "Setting up alarm for " + mainInfoModel.getLocationName());

        if (intentAction.equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.i("BootReceiver", "BOOT COMPLETED");
            Log.i("BootReceiver", String.valueOf(context.getSharedPreferences("my_shared_pref", Context.MODE_PRIVATE)
                    .getLong("alarm_interval", 60 * 1000)));

            currentConditionsAlarm.setupAlarm(
                    context.getSharedPreferences("my_shared_pref", Context.MODE_PRIVATE)
                    .getLong("alarm_interval", 60 * 1000)
            );
        }
    }

    private void updateModel(Context context) {
        //gets default preferences file name
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                "my_shared_pref", Context.MODE_PRIVATE
        );
        Log.i("BootReceiver", sharedPreferences.getString("city_name", "default_value"));
        MainInfoModel.locationName = sharedPreferences.getString("city_name", "default_value");
    }
}
