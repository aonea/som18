package com.endava.aonea.weatherapp.ui.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.endava.aonea.weatherapp.R;
import com.endava.aonea.weatherapp.alarm.CurrentConditionsAlarm;

public class SettingsActivity extends AppCompatActivity {
    public static String intervalMilliseconds = "60000";
    SharedPreferences sharedPreferences;
    CurrentConditionsAlarm currentConditionsAlarm;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        sharedPreferences = this.getSharedPreferences("my_shared_pref", Context.MODE_PRIVATE);
        currentConditionsAlarm = new CurrentConditionsAlarm(this);

        initViewsState();
        setListeners();
    }

    public void initViewsState() {
        ((Spinner) findViewById(R.id.intervalSpinner)).setSelection(
                sharedPreferences.getInt("interval_spinner_index", 0)
        );

        ((SwitchCompat) findViewById(R.id.notificationSwitch)).setChecked(
                sharedPreferences.getBoolean("notifications_on_boot", true)
        );
    }

    public void setListeners() {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        ((Spinner) findViewById(R.id.intervalSpinner)).setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        switch (position) {
                            case 0:
                                intervalMilliseconds = "60000";
                                editor.putLong("alarm_interval", Long.parseLong(intervalMilliseconds));
                                editor.apply();
                                break;
                            case 1:
                                intervalMilliseconds = "300000";
                                editor.putLong("alarm_interval", Long.parseLong(intervalMilliseconds));
                                editor.apply();
                                break;
                            case 2:
                                intervalMilliseconds = "900000";
                                editor.putLong("alarm_interval", Long.parseLong(intervalMilliseconds));
                                editor.apply();
                                break;
                            case 3:
                                intervalMilliseconds = "1800000";
                                editor.putLong("alarm_interval", Long.parseLong(intervalMilliseconds));
                                editor.apply();
                                break;
                        }

                        editor.putInt("interval_spinner_index", position);
                        editor.apply();
                        currentConditionsAlarm.resetAlarm(Long.parseLong(intervalMilliseconds));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );

        ((SwitchCompat) findViewById(R.id.notificationSwitch)).setOnCheckedChangeListener(
                (compoundButton, isChecked) -> {
                    if (!isChecked) {
                        currentConditionsAlarm.cancelAlarm();
                    } else {
                        currentConditionsAlarm.setupAlarm(Long.parseLong(intervalMilliseconds));
                    }

                    editor.putBoolean("notifications_on_boot", isChecked);
                    editor.apply();
                }
        );
    }
}
