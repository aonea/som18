package com.endava.aonea.weatherapp.ui.recyclerview;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.endava.aonea.weatherapp.R;
import com.endava.aonea.weatherapp.util.Util;

import java.util.List;

public class DaysAdapter extends RecyclerView.Adapter<DaysAdapter.DaysViewHolder> {
    private List<DayItem> daysList;
    private final OnItemClickListener onItemClickListener;

    public DaysAdapter(List<DayItem> daysList, OnItemClickListener onItemClickListener) {
        this.daysList = daysList;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public DaysViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.day_item, parent, false);
        return new DaysViewHolder(v);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull DaysAdapter.DaysViewHolder holder, int position) {
        holder.tvDayAbreviation.setText(daysList.get(position).getDay().substring(0, 2));
        holder.tvAvgTemperature.setText(
                String.valueOf(Util.fahrenheitToCelsius(Float.valueOf(daysList.get(position).getTemperature())))
        );
        holder.ivIcon.setImageResource(daysList.get(position).getIcon());
        if (holder.isSelected) {
            holder.clInnerCardLayout.setBackgroundColor(R.color.colorAccent);
        }
        holder.attachListener(daysList.get(position), onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return daysList.size();
    }

    public static class DaysViewHolder extends RecyclerView.ViewHolder {
        TextView tvDayAbreviation;
        public TextView tvAvgTemperature;
        ImageView ivIcon;
        ConstraintLayout clInnerCardLayout;
        boolean isSelected;
        static int lastPosition = -1;

        @SuppressLint("ResourceAsColor")
        DaysViewHolder(View itemView) {
            super(itemView);
            tvDayAbreviation = itemView.findViewById(R.id.tvDayAbrv);
            tvAvgTemperature = itemView.findViewById(R.id.tvAvgTemperature);
            ivIcon = itemView.findViewById(R.id.ivIcon);
            clInnerCardLayout = itemView.findViewById(R.id.clInnerCardLayout);
            if (itemView.isSelected()) {
                itemView.setBackgroundColor(R.color.colorAccent);
            }
        }

        void attachListener(DayItem dayItem, OnItemClickListener onItemClickListener) {
            itemView.setOnClickListener((l) -> {
                int currPosition = this.getAdapterPosition();
                Log.i("DaysViewHolder", Integer.toString(currPosition));
                Log.i("DaysViewHolder", Integer.toString(lastPosition));
                onItemClickListener.onItemClick(dayItem, currPosition, lastPosition);
                lastPosition = currPosition;
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(DayItem dayItem, int lastPosition, int currentPosition);
    }
}
