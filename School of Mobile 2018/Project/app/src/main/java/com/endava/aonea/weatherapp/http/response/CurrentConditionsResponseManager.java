package com.endava.aonea.weatherapp.http.response;

import android.util.Log;

import com.endava.aonea.weatherapp.util.Util;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

//TODO: fix redundant JSON parsing
public class CurrentConditionsResponseManager {
    private JSONObject responseJsonObject;
    private JSONObject mainJson;
    private String lastUpdateTime;

    public CurrentConditionsResponseManager(HttpURLConnection httpURLConnection) {
        try {
            InputStream contentInputStream = httpURLConnection.getInputStream();
            lastUpdateTime = httpURLConnection.getHeaderField("Date");
            String responseString = Util.convertIsToString(contentInputStream);
            responseJsonObject = new JSONObject(responseString);
            mainJson = (JSONObject) responseJsonObject.get("main");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getDay() {
        String date = "";

        try {
            date = responseJsonObject.getString("dt");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return Util.getDayFromEpoch(date);
    }

    public String getDate() {
        String date = "";

        try {
            date = responseJsonObject.getString("dt");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return Util.getDdMM(Long.valueOf(date) * 1000);
    }

    public String getCurrentTemp() {
        String currentTemp = "";

        try {
            currentTemp = mainJson.getString("temp");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return Util.kelvinToCelsius(Float.valueOf(currentTemp)) + "\u00b0";
    }

    public String getPressure() {
        String pressure = "";

        try {
            pressure = mainJson.getString("pressure");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return pressure + "mb";
    }

    public String getHumidity() {
        String humidity = "";

        try {
            humidity = mainJson.getString("humidity");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return humidity + "%";
    }

    public String getWind() {
        String wind = "";

        try {
            JSONObject windJson = (JSONObject) responseJsonObject.get("wind");
            wind = windJson.getString("speed");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return wind + "km/h";
    }

    public String getTime() {
        return "Last Update: " + lastUpdateTime.substring(
                lastUpdateTime.indexOf(' ')
        );
    }

    public String getDescription() {
        String description = "";

        try {
            JSONArray weatherArray = responseJsonObject.getJSONArray("weather");
            JSONObject todayJson = (JSONObject) weatherArray.get(0);
            description = todayJson.getString("description");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return description;
    }
}
