package com.endava.aonea.weatherapp.ui.animation;

import android.animation.ObjectAnimator;
import android.view.View;

import java.util.Arrays;

public class FadeInAnimation extends Animation{

    private float fromValue = 0.0f;
    private float toValue = 1.0f;
    private long duration = 1500;

    public FadeInAnimation(View... v){
        viewsToBeAnimated.addAll(Arrays.asList(v));
    }

    @Override
    public void setUpObjectAnimators() {
        for (View v :
                viewsToBeAnimated) {
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(v, "alpha", fromValue, toValue);
            objectAnimator.setDuration(duration);
            objectAnimatorList.add(objectAnimator);
        }
    }

    public void setFromValue(float fromValue) {
        this.fromValue = fromValue;
    }

    public void setToValue(float toValue) {
        this.toValue = toValue;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
