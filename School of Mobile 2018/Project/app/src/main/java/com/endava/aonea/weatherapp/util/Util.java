package com.endava.aonea.weatherapp.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Util {

    public static String convertIsToString(InputStream inputStream) {
        BufferedReader bufferedReader = null;
        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return stringBuilder.toString();
    }

    public static String getDayFromEpoch(String date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(Long.parseLong(date) * 1000));
        String day = "";
        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case 2:
                day = "MONDAY";
                break;
            case 3:
                day = "TUESDAY";
                break;
            case 4:
                day = "WEDNESDAY";
                break;
            case 5:
                day = "THURSDAY";
                break;
            case 6:
                day = "FRIDAY";
                break;
            case 7:
                day = "SATURDAY";
                break;
            case 1:
                day = "SUNDAY";
                break;
            default:
                return "ERR";
        }

        return day;
    }

    public static String getDdMm(String day) {
        return day.substring(day.lastIndexOf('-') + 1)
                + '/' +
                day.substring(day.indexOf('-') + 1, day.lastIndexOf('-'));
    }

    public static String getDdMM(Long epochDate) {
        Date date = new Date(epochDate);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");

        return sdf.format(date);
    }

    public static double fahrenheitToCelsius(float fahrenheit){
        return Math.floor(((fahrenheit - 32) * 5) / 9 * 10) / 10;
    }

    public static double kelvinToCelsius(float kelvin) {
        return Math.floor((kelvin - 273.15F) * 10) / 10;
    }
}
