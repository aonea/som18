package com.endava.aonea.weatherapp.ui.activity;

import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Messenger;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.endava.aonea.weatherapp.R;
import com.endava.aonea.weatherapp.http.request.service.CurrentConditionsService;
import com.endava.aonea.weatherapp.http.request.service.ForecastService;
import com.endava.aonea.weatherapp.job.CcJobService;
import com.endava.aonea.weatherapp.model.MainInfoModel;
import com.endava.aonea.weatherapp.ui.animation.FadeInAnimation;
import com.endava.aonea.weatherapp.ui.animation.TranslateUpAnimation;
import com.endava.aonea.weatherapp.ui.recyclerview.DayItem;
import com.endava.aonea.weatherapp.ui.recyclerview.DaysAdapter;
import com.endava.aonea.weatherapp.util.Util;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public List<DayItem> mDaysList = new ArrayList<>();
    private RecyclerView daysRecyclerView;
    private RecyclerView.Adapter<DaysAdapter.DaysViewHolder> daysAdapter;
    private RecyclerView.LayoutManager daysLayoutManager;
    private Handler currentConditionsMsgHandler;
    private ImageView ivCircle;
    private ImageView ivMainIcon;
    private View vSeparatorLine;
    private TextView tvTemperature;
    private TextView tvCurrentDay;
    private TextView tvCurrentDate;
    private ImageView ivRefreshIcon;
    private TextView tvLastUpdateTime;
    private Spinner locationSpinner;
    private TextView tvPressureValue;
    private TextView tvHumidityValue;
    private TextView tvWindValue;
    private ImageView ivPreferences;

    public static final String IASI_LOCATION_CODE = "287994";
    public static final String PARIS_LOCATION_CODE = "1571845";
    public static final String BERN_LOCATION_CODE = "312122";

    //mock got from db
    private HashMap<Integer, String> indexLocationMap = new HashMap<>();
    public static String locationCode = IASI_LOCATION_CODE;
    static MainInfoModel mainInfoModel = MainInfoModel.getInstance();

    //TODO: separate into ui package
    //TODO: m's everywhere
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new ForecastReceiver(new WeakReference<>(this)), new IntentFilter(ForecastService.ACTION_FORECAST_UPDATE)
        );

        currentConditionsMsgHandler = new Handler((message) -> {
            Log.i("MsgHandler", "got message back");

            tvCurrentDay.setText(mainInfoModel.getDay());
            tvCurrentDate.setText(mainInfoModel.getDate());
            tvTemperature.setText(mainInfoModel.getCurrentTemp());
            tvLastUpdateTime.setText(mainInfoModel.getTime());
            tvPressureValue.setText(mainInfoModel.getPressure());
            tvWindValue.setText(mainInfoModel.getWind());
            tvHumidityValue.setText(mainInfoModel.getHumidity());

            return true;
        });
        getSavedLocations();
        playInitialAnimation();
        setListeners();
//TODO:        setUpJobService();
    }

    private void initViews() {
        ivCircle = findViewById(R.id.ivCircle);
        ivMainIcon = findViewById(R.id.ivMainIcon);
        vSeparatorLine = findViewById(R.id.separatorLine);
        tvTemperature = findViewById(R.id.tvTemperature);
        tvCurrentDay = findViewById(R.id.tvCurrentDay);
        tvCurrentDate = findViewById(R.id.tvCurrentDate);
        ivRefreshIcon = findViewById(R.id.ivRefreshIcon);
        tvLastUpdateTime = findViewById(R.id.tvLastUpdateTime);
        locationSpinner = findViewById(R.id.locationSpinner);
        tvHumidityValue = findViewById(R.id.tvHumidityValue);
        tvWindValue = findViewById(R.id.tvWindValue);
        tvPressureValue = findViewById(R.id.tvPressureValue);
        ivPreferences = findViewById(R.id.ivPreferences);
        loadSpinnerState();
    }

    public void getSavedLocations() {
        //dummy retrieval from db
        indexLocationMap.put(0, IASI_LOCATION_CODE);
        indexLocationMap.put(1, PARIS_LOCATION_CODE);
        indexLocationMap.put(2, BERN_LOCATION_CODE);
    }

    private void startCurrentConditionsService() {
        Intent intent = new Intent(this, CurrentConditionsService.class);
        intent.putExtra("messenger", new Messenger(currentConditionsMsgHandler));
        startService(intent);
    }

    private void startForecastService() {
        Intent forecastIntent = new Intent(this, ForecastService.class);
        startService(forecastIntent);
    }

    private void setListeners() {
        ivRefreshIcon.setOnClickListener((l) -> {
            FadeInAnimation fadeInAnimation = new FadeInAnimation(
                    ivMainIcon,
                    tvCurrentDay,
                    tvCurrentDate,
                    tvTemperature,
                    tvLastUpdateTime,
                    tvHumidityValue,
                    tvPressureValue,
                    tvWindValue
            );
            fadeInAnimation.setUpObjectAnimators();
            fadeInAnimation.setPlayTogether();
            fadeInAnimation.animatorSet.start();

            startCurrentConditionsService();
            startForecastService();
        });

        ivPreferences.setOnClickListener((l) -> {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        });

        locationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            WeakReference<MainActivity> mainActivityWeakReference =
                    new WeakReference<>(MainActivity.this);


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                MainInfoModel.locationCode = indexLocationMap.get(position);
                mainInfoModel.setLocationName(parent.getItemAtPosition(position).toString());
                mainActivityWeakReference.get().persistLocation(parent.getItemAtPosition(position).toString());
                mainActivityWeakReference.get().persistSpinnerState(position);
                startCurrentConditionsService();
                startForecastService();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setUpRecyclerView() {
        daysRecyclerView = findViewById(R.id.rvDays);
        daysRecyclerView.setHasFixedSize(true);

        daysLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        daysRecyclerView.setLayoutManager(daysLayoutManager);


        daysAdapter = new DaysAdapter(
                mDaysList,
                (dayItem, currentPosition, lastPosition) -> {
                    //TODO: FIX SELECTION BUG
                    //TODO: SPLIT into two methods
                    ((TextView) findViewById(R.id.tvCurrentDay)).setText(dayItem.getDay());
                    ((ImageView) findViewById(R.id.ivMainIcon)).setImageResource(dayItem.getIcon());
                    ((TextView) findViewById(R.id.tvTemperature)).setText(
                            String.valueOf(
                                    Util.fahrenheitToCelsius(
                                            (Float.valueOf(dayItem.getTemperature()))
                                    )) + "\u00b0");
                    ((TextView) findViewById(R.id.tvCurrentDate)).setText(Util.getDdMm(dayItem.getDate()));
                    mDaysList.get(currentPosition).setSelected(true);
                    if (lastPosition != -1) {
                        mDaysList.get(lastPosition).setSelected(false);
                    }
                    daysAdapter.notifyDataSetChanged();
                }
        );
        daysRecyclerView.setAdapter(daysAdapter);
    }

    private void playInitialAnimation() {
        TranslateUpAnimation translateUpAnimation = new TranslateUpAnimation(
                ivCircle,
                vSeparatorLine,
                tvTemperature,
                tvCurrentDate,
                tvCurrentDay
        );
        translateUpAnimation.setUpObjectAnimators();
        translateUpAnimation.setPlayTogether();

        FadeInAnimation fadeInAnimation = new FadeInAnimation(
                ivMainIcon,
                tvCurrentDay,
                tvCurrentDate,
                tvTemperature,
                tvLastUpdateTime,
                tvWindValue,
                tvPressureValue,
                tvHumidityValue
        );
        fadeInAnimation.setUpObjectAnimators();
        fadeInAnimation.setPlayTogether();

        AnimatorSet animatorSetPlayer = new AnimatorSet();
        animatorSetPlayer.playSequentially(translateUpAnimation.animatorSet, fadeInAnimation.animatorSet);
        animatorSetPlayer.start();
    }

    public static class ForecastReceiver extends BroadcastReceiver {
        WeakReference<MainActivity> mainActivityWeakReference;

        public ForecastReceiver(WeakReference<MainActivity> mainActivityWeakReference) {
            this.mainActivityWeakReference = mainActivityWeakReference;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String intentAction = intent.getAction();

            if (intentAction.equals(ForecastService.ACTION_FORECAST_UPDATE)) {
                mainActivityWeakReference.get().mDaysList = ForecastService.forecastList;
                mainActivityWeakReference.get().setUpRecyclerView();
            }
        }
    }

    public boolean checkNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setUpJobService() {
        CcJobService ccJobService = new CcJobService();

        ccJobService.scheduleJob(this);
    }

    public void persistLocation(String cityName) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("my_shared_pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("city_name", cityName);
        editor.apply();
    }

    public void persistSpinnerState(int position) {
        SharedPreferences sharedPreferences = this.getSharedPreferences(
                "my_shared_pref", Context.MODE_PRIVATE
        );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("spinner_selection_index", position);
        editor.apply();
    }

    public void loadSpinnerState() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(
                "my_shared_pref", Context.MODE_PRIVATE
        );
        locationSpinner.setSelection(
                sharedPreferences.getInt("spinner_selection_index", 0)
        );
    }
}
