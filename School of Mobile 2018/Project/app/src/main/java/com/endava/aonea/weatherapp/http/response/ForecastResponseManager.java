package com.endava.aonea.weatherapp.http.response;

import android.util.Log;

import com.endava.aonea.weatherapp.R;
import com.endava.aonea.weatherapp.ui.recyclerview.DayItem;
import com.endava.aonea.weatherapp.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

public class ForecastResponseManager {
    private JSONArray forecastJsonArray;
    private final int FORECAST_DAYS = 5;
    JSONObject responseJson;

    public ForecastResponseManager(HttpURLConnection httpURLConnection) {
        try {
            InputStream contentInputStream = httpURLConnection.getInputStream();
            String responseString = Util.convertIsToString(contentInputStream);
            responseJson = new JSONObject(responseString);
            forecastJsonArray = responseJson.getJSONArray("DailyForecasts");
            getDayList();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getHeadline() throws JSONException {
        String text = "";

        try {
            JSONObject headlineJson = (JSONObject) responseJson.get("Headline");
            text = headlineJson.getString("Text");

        } catch (NullPointerException e){
            Log.e("getHeadline()", "Ran out of API queries, using dummy");
            text = "Clouds and rain.";
        }

        return text;
    }

    public List<DayItem> getDayList() throws JSONException {
        List<DayItem> days = new ArrayList<>();

        JSONObject dayJson = new JSONObject();
        JSONObject tempJson = new JSONObject();
        JSONObject maxTempJson = new JSONObject();

        for (int i = 0; i <= FORECAST_DAYS - 1; i++) {
            try {
                dayJson = (JSONObject) forecastJsonArray.get(i);
                tempJson = (JSONObject) dayJson.get("Temperature");
                maxTempJson = (JSONObject) tempJson.get("Maximum");
                String date = dayJson.getString("Date");

                days.add(
                        new DayItem(
                                Util.getDayFromEpoch(dayJson.getString("EpochDate")),
                                R.drawable.cloudy_icon,
                                maxTempJson.getString("Value"),
                                date.substring(0, date.indexOf('T'))
                        )
                );
            } catch (NullPointerException e) {
                //set dummies in case I run out of API calls
                Log.e("ForecastResponseManager", "Out of API calls, using dummy");

                days.add(new DayItem("FR", R.drawable.cloudy_icon, "21", "2018-05-25"));
                days.add(new DayItem("SA", R.drawable.cloudy_icon, "15", "2018-05-26"));
                days.add(new DayItem("SU", R.drawable.cloudy_icon, "26", "2018-05-27"));
                days.add(new DayItem("MO", R.drawable.cloudy_icon, "30", "2018-05-28"));
                days.add(new DayItem("TU", R.drawable.cloudy_icon, "24", "2018-05-29"));

                return days;
            }
        }

        return days;
    }
}
