package com.endava.aonea.weatherapp.ui.animation;

import android.animation.ObjectAnimator;
import android.view.View;

import java.util.Arrays;

public class TranslateUpAnimation extends Animation {
    private float startPosition = 250f;
    private float endPosition = 0f;
    private long duration = 700;

    public TranslateUpAnimation(View... views) {
        viewsToBeAnimated.addAll(Arrays.asList(views));
    }

    @Override
    public void setUpObjectAnimators() {
        for (View v : viewsToBeAnimated) {
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(v, "translationY", startPosition, endPosition);
            objectAnimator.setDuration(duration);
            objectAnimatorList.add(objectAnimator);
        }
    }

    public void setStartPostion(float startPostion) {
        this.startPosition = startPostion;
    }

    public void setEndPosition(float endPosition) {
        this.endPosition = endPosition;
    }


    public void setDuration(long duration) {
        this.duration = duration;
    }

}
