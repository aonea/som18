package com.endava.aonea.weatherapp.ui.animation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.view.View;

import java.util.LinkedList;
import java.util.List;

abstract class Animation {
    //TODO: m's everywhere
    protected LinkedList<View> viewsToBeAnimated = new LinkedList<>();
    protected List<Animator> objectAnimatorList = new LinkedList<>();
    public AnimatorSet animatorSet = new AnimatorSet();

    abstract void setUpObjectAnimators();

    public void setPlayTogether(){
        animatorSet.playTogether(objectAnimatorList);
    }
}
