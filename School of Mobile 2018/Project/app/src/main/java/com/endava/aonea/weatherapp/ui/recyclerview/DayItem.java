package com.endava.aonea.weatherapp.ui.recyclerview;

public class DayItem {
    private String day;
    private String temperature;
    private String date;
    private int icon;
    private int layout;
    private boolean isSelected;

    public DayItem(String day, int icon, String temperature, String date) {
        this.day = day;
        this.icon = icon;
        this.temperature = temperature;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
