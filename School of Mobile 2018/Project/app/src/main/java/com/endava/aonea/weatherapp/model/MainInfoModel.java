package com.endava.aonea.weatherapp.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainInfoModel {
    private static MainInfoModel mainInfoModel = new MainInfoModel();

    private String day;
    private String date;
    private String currentTemp;
    private String time;
    private String description;
    private String pressure;
    private String wind;
    private String humidity;

    public static final String IASI_LOCATION_CODE = "287994";
    public static final String PARIS_LOCATION_CODE = "1571845";
    public static final String BERN_LOCATION_CODE = "312122";

    public static String locationCode = IASI_LOCATION_CODE;

    public static String locationName;

    private MainInfoModel() {

    }

    public static MainInfoModel getInstance() {
        return mainInfoModel;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        MainInfoModel.locationName = locationName;
    }

    public String getDay() {
        return mainInfoModel.day;
    }

    public void setDay(String day) {
        mainInfoModel.day = day;
    }

    public String getDate() {
        return mainInfoModel.date;
    }

    public void setDate(String date) {
        mainInfoModel.date = date;
    }

    public String getCurrentTemp() {
        return mainInfoModel.currentTemp;
    }

    public void setCurrentTemp(String currentTemp) {
        mainInfoModel.currentTemp = currentTemp;
    }

    public String getTime() {
        return mainInfoModel.time;
    }

    public void setTime(String time) {
        mainInfoModel.time = time;
    }

    public String getDescription() {
        return mainInfoModel.description;
    }

    public void setDescription(String description) {
        mainInfoModel.description = description;
    }

    public String getPressure() {
        return mainInfoModel.pressure;
    }

    public void setPressure(String pressure) {
        mainInfoModel.pressure = pressure;
    }

    public String getWind() {
        return mainInfoModel.wind;
    }

    public void setWind(String wind) {
        mainInfoModel.wind = wind;
    }

    public String getHumidity() {
        return mainInfoModel.humidity;
    }

    public void setHumidity(String humidity) {
        mainInfoModel.humidity = humidity;
    }
}
