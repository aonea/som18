package com.endava.aonea.weatherapp.job;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.Toast;

import com.endava.aonea.weatherapp.http.request.service.CurrentConditionsService;
import com.endava.aonea.weatherapp.ui.activity.SettingsActivity;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class CcJobService extends JobService {

    @Override
    public boolean onStartJob(JobParameters params) {
        Intent intent = new Intent(this, CurrentConditionsService.class);
        intent.putExtra("caller", "boot_receiver");
        startService(intent);
        Toast.makeText(this, "Job Started", Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

    public void scheduleJob(Context context) {
        JobInfo.Builder builder = new JobInfo.Builder(
                1,
                new ComponentName(context.getPackageName(), CcJobService.class.getName())
        )
                .setPeriodic(Long.parseLong(SettingsActivity.intervalMilliseconds))
                .setPersisted(true)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);

        JobInfo jobInfo = builder.build();

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(jobInfo);
    }

}
