package com.endava.aonea.weatherapp.http.request;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.endava.aonea.weatherapp.model.MainInfoModel;
import com.endava.aonea.weatherapp.ui.activity.MainActivity;

import java.net.MalformedURLException;
import java.net.URL;

import static com.endava.aonea.weatherapp.model.MainInfoModel.locationName;

public class RequestManager {
    private static final String FORECAST_URL = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/";
    private static final String CURRENT_CONDITIONS_URL = "http://api.openweathermap.org/data/2.5/weather?q=";
    private static final String API_KEY = "QB90uAtVGGgAe79OA2E6n7eRRgVYV2Hw";
    private static final String API_KEY_OWM = "d352e3bd8f081e0fe49d5b40e11fa29c";
    static MainInfoModel mainInfoModel = MainInfoModel.getInstance();

    public static URL getRequestUrlCurr(String locationName) {
        URL requestUrl = null;
        try {
            requestUrl = new URL(CURRENT_CONDITIONS_URL + locationName + "&appid=" + API_KEY_OWM);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return requestUrl;
    }

    public static URL getRequestUrlForecast(String locationCode) {
        URL requestUrl = null;

        try {
            requestUrl = new URL(FORECAST_URL + '/' + MainInfoModel.locationCode + "?apikey=" + API_KEY);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return requestUrl;
    }
}
