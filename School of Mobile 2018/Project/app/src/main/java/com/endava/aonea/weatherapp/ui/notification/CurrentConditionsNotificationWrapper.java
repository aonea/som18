package com.endava.aonea.weatherapp.ui.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.endava.aonea.weatherapp.R;
import com.endava.aonea.weatherapp.ui.activity.MainActivity;
import com.endava.aonea.weatherapp.ui.activity.SettingsActivity;

import static android.content.Context.NOTIFICATION_SERVICE;
import static android.support.v4.app.NotificationCompat.DEFAULT_SOUND;
import static android.support.v4.app.NotificationCompat.VISIBILITY_PUBLIC;

public class CurrentConditionsNotificationWrapper {
    private static final int NOTIFICATION_ID = 2;
    private static final String CHANNEL_ID = "2";
    private static NotificationChannel notificationChannel;
    private NotificationManager notificationManager;
    private Context context;

    public CurrentConditionsNotificationWrapper(Context context) {
        this.context = context;
        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "CurrentConditions",
                    NotificationManager.IMPORTANCE_HIGH
            );
        }

        notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
    }

    public void notifyUser(String locationName, String currentTemp, String description){
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.sun_icon);
        PendingIntent contentIntent = PendingIntent.getActivity(
                context,
                0,
                new Intent(context, MainActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        Intent intervalIntent = new Intent(context, SettingsActivity.class);
        PendingIntent intervalPendingIntent = PendingIntent.getActivity(context, 0, intervalIntent, 0);

        notificationManager.notify(
                NOTIFICATION_ID,
                new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setDefaults(DEFAULT_SOUND)
                        .setContentText(locationName + ": " + currentTemp + ". " + description)
                        .setSmallIcon(R.mipmap.ic_sun_icon)
                        .setContentTitle("Current")
                        //TODO: get icon from API
                        .setLargeIcon(bitmap)
                        .setVisibility(VISIBILITY_PUBLIC)
                        .setVibrate(new long[] {100, 100, 100, 100})
                        .setLights(Color.GREEN, 1000, 1000)
                        .setContentIntent(contentIntent)
                        .addAction(R.drawable.ic_launcher_foreground, "SET INTERVAL", intervalPendingIntent)
                        //TODO: get headline from API
                        .build()
        );
    }
}
