package com.endava.aonea.weatherapp.http.connection;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

public class ConnectionWrapper {
    public HttpURLConnection httpURLConnection = null;
    private URL url;

    public ConnectionWrapper(URL url) {
        this.url = url;
    }

    public HttpURLConnection getConnection() {
        try {
            httpURLConnection = (HttpURLConnection) (url).openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        httpURLConnection.setReadTimeout(10000);
        httpURLConnection.setConnectTimeout(15000);
        try {
            httpURLConnection.setRequestMethod("GET");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        httpURLConnection.setDoInput(true);

        return httpURLConnection;
    }
}
