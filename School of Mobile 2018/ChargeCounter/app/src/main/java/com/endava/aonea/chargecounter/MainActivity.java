package com.endava.aonea.chargecounter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registerReceiver(new PowerConnectedReceiver(this), new IntentFilter(Intent.ACTION_POWER_CONNECTED));
    }

    public static class PowerConnectedReceiver extends BroadcastReceiver {
        private static int counter = 0;
        private AppCompatActivity callerActivity;

        public PowerConnectedReceiver(AppCompatActivity callerActivity) {
            this.callerActivity = callerActivity;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String intentAction = intent.getAction();
            switch (intentAction) {
                case Intent.ACTION_POWER_CONNECTED: {
                    Toast.makeText(context, "Power Connected", Toast.LENGTH_SHORT).show();
                    ((TextView) callerActivity.findViewById(R.id.tvCounter)).setText(
                            String.valueOf(++counter)
                    );
                }
                break;
            }
        }
    }
}
